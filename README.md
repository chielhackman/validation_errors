# ValidationErrors
Instead of showing a list of all errors, get the validation error message from a specific attribute and display it where you want it.

## Usage
Add the desired [validations](https://guides.rubyonrails.org/active_record_validations.html) to your model.

Below your input field in a form call the helper `error_message` and pass it 2 arguments, the object and the attribute.

This will add an `div` with inside an `small` tag displaying the validation error message. The `small` tag will have a class `text-danger`.

## Examples

```ruby
# Post model
Class Post < ApplicationRecord
  validates_presence_of :name
end

# Haml
= form_for(@post) do |f|
  = f.label :name
  = f.text_field :name
  = error_message(@post, :name)

# ERB
<%= form_for(@post) do |f| %>
  <%= f.label :name %>
  <%= f.text_field :name %>
  <%= error_message(@post, :name) %>
<% end %>
```

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'validation_errors', git: 'https://gitlab.com/chielhackman/validation_errors'
```

And then execute:
```bash
$ bundle
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
