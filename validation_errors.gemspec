$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "validation_errors/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "validation_errors"
  spec.version     = ValidationErrors::VERSION
  spec.authors     = ["Chiel Hackman"]
  spec.email       = ["chielhackman@gmail.com"]
  spec.homepage    = "https://gitlab.com/chielhackman/validation_errors"
  spec.summary     = "Get the error message from a specific attribute out of the errors array."
  spec.description = "Instead of showing a list of all errors, get the error message from a specific attribute and display it where you want it."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 6.0.2", ">= 6.0.2.1"

  spec.add_development_dependency "sqlite3"
end
