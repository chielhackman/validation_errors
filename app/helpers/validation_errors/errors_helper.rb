module ValidationErrors
  module ErrorsHelper
    def error_message(object, field)
      return unless object.errors[:"#{field}"]

      content_tag :div do
        object.errors[:"#{field}"].each do |msg|
          concat content_tag(:small, msg, class: 'text-danger')
        end
      end
    end
  end
end
